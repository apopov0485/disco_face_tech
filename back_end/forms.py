from django import forms
from .models import ImageUpload, ImageResult, VideoUpload, DeepFaceUpload


class ImageUploadForm(forms.ModelForm):
    class Meta:
        model = ImageUpload
        fields = ['name', 'up_image']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter a name for the image'}),
            'up_image': forms.FileInput(attrs={'class': 'btn btn-primary'}),
        }


class ResultImageForm(forms.ModelForm):
    class Meta:
        model = ImageResult
        fields = ['result', 'num_people', 'store_image']


class VideoForm(forms.ModelForm):
    class Meta:
        model = VideoUpload
        fields = ["name", "video_file"]
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter a name for the video'}),
            'video_file': forms.FileInput(attrs={'class': 'btn btn-primary'}),
        }


class HiddenTakeScreenshotForm(forms.Form):
    take_pic = forms.BooleanField()#widget=forms.HiddenInput


class DeepFaceUploadForm(forms.ModelForm):
    class Meta:
        model = DeepFaceUpload
        fields = ['up_image']
        widgets = {
            'up_image': forms.FileInput(attrs={'class': 'btn btn-primary'}),
        }

