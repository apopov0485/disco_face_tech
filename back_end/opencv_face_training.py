import os
import cv2 as cv
import numpy as np
from django.conf import settings
from .models import Client, ModelOpenCV
from django.core.files.storage import default_storage

people = ['AlPacino', 'Ben Afflek', 'Elton John', 'Jerry Seinfield', 'Madonna', 'Mindy Kaling']
DIR = os.path.join(settings.BASE_DIR, 'dataset/Faces/train')
# DIR = r'../dataset/Faces/train'

haar_cascade = cv.CascadeClassifier(os.path.join(
    settings.BASE_DIR, 'opencv_haarcascade_data/haarcascade_frontalface_default.xml'))
# haar_cascade = cv.CascadeClassifier(r'./opencv_haarcascade_data/haarcascade_frontalface_default.xml')


def create_train():
    features = []
    labels = []
    Client.objects.all().delete()
    ModelOpenCV.objects.all().delete()
    for person in people:
        path = os.path.join(DIR, person)
        label = people.index(person)
        Client(name=person).save()
        # ModelOpenCV(name=Client.objects.get(name=person), saved_model='face_trained.yml').save()
        for img in os.listdir(path):
            img_path = os.path.join(path, img)

            img_array = cv.imread(img_path)
            if img_array is None:
                continue

            gray = cv.cvtColor(img_array, cv.COLOR_BGR2GRAY)

            faces_rect = haar_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=4)

            for (x, y, w, h) in faces_rect:
                faces_roi = gray[y:y + h, x:x + w]
                features.append(faces_roi)
                labels.append(label)

    features = np.array(features, dtype='object')
    labels = np.array(labels)

    face_recognizer = cv.face.LBPHFaceRecognizer_create()

    # Train the Recognizer on the features list and the labels list
    face_recognizer.train(features, labels)
    face_recognizer.save('face_trained.yml')
    # default_storage.save('face_trained.yml', 'face_trained.yml')

    # db_save = ModelOpenCV.objects.filter(saved_model='face_trained1.yml').first()
    # print(face_recognizer.save('face_trained.yml'))
    # db_save.saved_model.save('face_trained.yml', face_recognizer.save('face_trained1.yml'))
    # np.save('features.npy', features)
    # np.save('labels.npy', labels)
