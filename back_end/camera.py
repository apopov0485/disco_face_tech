import cv2
import os
import numpy as np
from django.conf import settings
import mediapipe as mp
from mtcnn_cv2 import MTCNN
import random
from datetime import datetime
from django.http import request
from django.http.response import JsonResponse
from back_end.models import ModelOpenCV

face_detection_videocam = cv2.CascadeClassifier(os.path.join(
    settings.BASE_DIR, 'opencv_haarcascade_data/haarcascade_frontalface_default.xml'))


class VideoCamera(object):
    screenshot = False

    def __init__(self):
        self.video = cv2.VideoCapture(0)
        self.request = request


    def __del__(self):
        self.video.release()

    def take_screenshot(self, screenshot=False):
        if screenshot:
            print('*' * 50)
            VideoCamera.screenshot = True

    def get_frame(self):
        success, image = self.video.read()

        if not success:
            return None
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces_detected = face_detection_videocam.detectMultiScale3(gray, scaleFactor=1.3, minNeighbors=3,
                                                                   outputRejectLevels=True)
        if VideoCamera.screenshot and len(faces_detected[0]) != 0:
            print('ok')
            VideoCamera.screenshot = False
            # print(settings.DATASET)
            screenshot_name = settings.DATASET + 'shot_' + str(random.randint(1, 9999)) + \
                              datetime.now().strftime("%Y%m%d%H%M%S") + str(random.randint(1, 99)) + '.jpg'
            cv2.imwrite(screenshot_name, image)  # Store as image

            screenshot_name = '/' + screenshot_name
            # shot = ModelOpenCV(photo=screenshot_name, name=173
            #                     )
            # shot.save()

        for (x, y, w, h) in faces_detected[0]:
            bbox = x, y, w, h
            self.fancy_draw(image, bbox)
        frame_flip = cv2.flip(image, 1)
        ret, jpeg = cv2.imencode('.jpg', frame_flip)
        return jpeg.tobytes()

    def fancy_draw(self, img, bbox, l=30, t=5, rt=1):
        x, y, w, h = bbox
        x1, y1 = x + w, y + h

        cv2.rectangle(img, bbox, (255, 0, 255), rt)
        # Top Left  x,y
        cv2.line(img, (x, y), (x + l, y), (255, 0, 255), t)
        cv2.line(img, (x, y), (x, y + l), (255, 0, 255), t)
        # Top Right  x1,y
        cv2.line(img, (x1, y), (x1 - l, y), (255, 0, 255), t)
        cv2.line(img, (x1, y), (x1, y + l), (255, 0, 255), t)
        # Bottom Left  x,y1
        cv2.line(img, (x, y1), (x + l, y1), (255, 0, 255), t)
        cv2.line(img, (x, y1), (x, y1 - l), (255, 0, 255), t)
        # Bottom Right  x1,y1
        cv2.line(img, (x1, y1), (x1 - l, y1), (255, 0, 255), t)
        cv2.line(img, (x1, y1), (x1, y1 - l), (255, 0, 255), t)
        return img


class FaceDetector(object):
    def __init__(self, minDetectionCon=0.6):

        self.minDetectionCon = minDetectionCon
        self.mpFaceDetection = mp.solutions.face_detection
        self.mpDraw = mp.solutions.drawing_utils
        self.faceDetection = self.mpFaceDetection.FaceDetection(self.minDetectionCon, model_selection=1)
        self.video = cv2.VideoCapture(0)

    def findFaces(self, img, draw=True):

        imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.results = self.faceDetection.process(imgRGB)
        bboxs = []
        if self.results.detections:
            for id, detection in enumerate(self.results.detections):
                bboxC = detection.location_data.relative_bounding_box
                ih, iw, ic = img.shape
                bbox = int(bboxC.xmin * iw), int(bboxC.ymin * ih), \
                       int(bboxC.width * iw), int(bboxC.height * ih)
                bboxs.append([id, bbox, detection.score])
                if draw:
                    img = self.fancyDraw(img, bbox)
                    img = cv2.flip(img, 1)
                    cv2.putText(img, f'{int(detection.score[0] * 100)}%',
                                (bbox[0], bbox[1] - 20), cv2.FONT_HERSHEY_PLAIN,
                                2, (255, 0, 255), 2)

        return img, bboxs

    def fancyDraw(self, img, bbox, l=30, t=5, rt=1):
        x, y, w, h = bbox
        x1, y1 = x + w, y + h

        cv2.rectangle(img, bbox, (255, 0, 255), rt)
        # Top Left  x,y
        cv2.line(img, (x, y), (x + l, y), (255, 0, 255), t)
        cv2.line(img, (x, y), (x, y + l), (255, 0, 255), t)
        # Top Right  x1,y
        cv2.line(img, (x1, y), (x1 - l, y), (255, 0, 255), t)
        cv2.line(img, (x1, y), (x1, y + l), (255, 0, 255), t)
        # Bottom Left  x,y1
        cv2.line(img, (x, y1), (x + l, y1), (255, 0, 255), t)
        cv2.line(img, (x, y1), (x, y1 - l), (255, 0, 255), t)
        # Bottom Right  x1,y1
        cv2.line(img, (x1, y1), (x1 - l, y1), (255, 0, 255), t)
        cv2.line(img, (x1, y1), (x1, y1 - l), (255, 0, 255), t)
        return img

    def get_frame_mp(self):
        success, img = self.video.read()
        img, bboxs = self.findFaces(img)
        ret, jpeg = cv2.imencode('.jpg', img)
        return jpeg.tobytes()


class FaceDetectorMTCNN(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)

    def __del__(self):
        self.video.release()

    def get_frame_mtcnn(self):
        success, image = self.video.read()
        detector = MTCNN()
        img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        result = detector.detect_faces(img)
        for id, res in enumerate(result):
            x, y, w, h = res['box']

            self.fancy_draw(image, (x, y, w, h))
            frame_flip = cv2.flip(image, 1)
            cv2.putText(frame_flip, f'{int(res["confidence"] * 100)}%',
                        (x, y - 20), cv2.FONT_HERSHEY_PLAIN,
                        2, (255, 0, 255), 2)

            ret, jpeg = cv2.imencode('.jpg', frame_flip)
            return jpeg.tobytes()

    def fancy_draw(self, img, bbox, l=30, t=5, rt=1):
        x, y, w, h = bbox
        x1, y1 = x + w, y + h

        cv2.rectangle(img, bbox, (255, 0, 255), rt)
        # Top Left  x,y
        cv2.line(img, (x, y), (x + l, y), (255, 0, 255), t)
        cv2.line(img, (x, y), (x, y + l), (255, 0, 255), t)
        # Top Right  x1,y
        cv2.line(img, (x1, y), (x1 - l, y), (255, 0, 255), t)
        cv2.line(img, (x1, y), (x1, y + l), (255, 0, 255), t)
        # Bottom Left  x,y1
        cv2.line(img, (x, y1), (x + l, y1), (255, 0, 255), t)
        cv2.line(img, (x, y1), (x, y1 - l), (255, 0, 255), t)
        # Bottom Right  x1,y1
        cv2.line(img, (x1, y1), (x1 - l, y1), (255, 0, 255), t)
        cv2.line(img, (x1, y1), (x1, y1 - l), (255, 0, 255), t)
        return img
