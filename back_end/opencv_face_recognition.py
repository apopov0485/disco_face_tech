import numpy as np
import cv2 as cv
import os
from django.conf import settings
# from models import Client, ModelOpenCV
from django.core.files.base import ContentFile


haar_cascade = cv.CascadeClassifier('../opencv_haarcascade_data/haarcascade_frontalface_default.xml')
# haar_cascade = cv.CascadeClassifier(os.path.join(
#     settings.BASE_DIR, 'opencv_haarcascade_data/haarcascade_frontalface_default.xml'))
people = ['al_pacino', 'ben_afflek', 'elton_john', 'jerry_seinfeld', 'madonna', 'mindy_kaling']
# people = ['Al Pacino', 'Ben Afflek', 'Elton John', 'Jerry Seinfield', 'Madonna', 'Mindy Kaling']
# features = np.load('features.npy', allow_pickle=True)
# labels = np.load('labels.npy')

face_recognizer = cv.face.LBPHFaceRecognizer_create()

# face_recognizer.read(os.path.join(settings.MEDIA_ROOT, 'face_trained.yml'))
face_recognizer.read('../media/face_trained.yml')
DIR = '../dataset/Faces/val/'
# DIR = os.path.join(settings.BASE_DIR, 'dataset/Faces/val')

# data = ModelOpenCV()

#
# def predict():
#     for person in people:
#         path = os.path.join(DIR, person)
#
#         label = people.index(person)
#
#         for img in os.listdir(path):
#             img_path = os.path.join(path, img)
#             print(img_path)
#             img = cv.imread(img_path)
#             gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
#             # Detect the face in the image
#             faces_rect = haar_cascade.detectMultiScale(gray, 1.1, 4)
#             conf = ''
#             for (x,y,w,h) in faces_rect:
#                 faces_roi = gray[y:y+h,x:x+w]
#
#                 label, confidence = face_recognizer.predict(faces_roi)
#                 # print(f'Label = {people[label]} with a confidence of {confidence}')
#                 conf = confidence
#                 cv.putText(img, str(people[label]), (20,20), cv.FONT_HERSHEY_COMPLEX, 1.0, (0,255,0), thickness=2)
#                 cv.rectangle(img, (x,y), (x+w,y+h), (0,255,0), thickness=2)
#             ret, buf = cv.imencode('.jpg', img)
#             content = ContentFile(buf.tobytes())
#             print(person)
#                 # ModelOpenCV(name=Client.objects.get(name=person), saved_model='face_trained.yml', confidence=confidence)
#             # data.name = Client.objects.get(name=person)
#             # data.confidence = conf
#             # data.saved_model = 'face_trained.yml'
#             # data.photo.save(f'opencv_result_{conf}.jpg', content)
#             cv.imshow('Detected Face', img)
#
#             cv.waitKey(0)
# predict()

clients = []

for r, d, f in os.walk(DIR):  # r=root, d=directories, f = files
    for file in f:
        if ('.jpg' in file):
            exact_path = r + "/" + file
            clients.append(exact_path)


def save_result():
    for client in clients:
            img = cv.imread(client)
            if img is not None:

                gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            # Detect the face in the image
                faces_rect = haar_cascade.detectMultiScale(gray, 1.1, 4)
                conf = ''
                for (x, y, w, h) in faces_rect:
                    faces_roi = gray[y:y + h, x:x + w]

                    label, confidence = face_recognizer.predict(faces_roi)
                    conf = confidence
                    cv.putText(img, str(people[label]), (20, 20), cv.FONT_HERSHEY_COMPLEX, 1.0, (0, 255, 0), thickness=1)
                    cv.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), thickness=1)
                name = client.split('/')[4]

                cv.imwrite(f'../media/opencv/result/{name}-{conf}.jpg', img)
# save_result()