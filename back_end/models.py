from django.db import models


class ImageUpload(models.Model):
    name = models.CharField(max_length=50)
    up_image = models.ImageField(upload_to='upload/')


class VideoUpload(models.Model):
    name = models.CharField(max_length=50)
    video_file = models.FileField(upload_to='videos/', null=True, verbose_name="")


class ImageResult(models.Model):
    result = models.ForeignKey(ImageUpload, on_delete=models.CASCADE)
    num_people = models.IntegerField(default=0)
    store_image = models.ImageField(upload_to='result/')
    opencv = models.BooleanField(default=False)
    mediapipe = models.BooleanField(default=False)
    mtcnn_cv2 = models.BooleanField(default=False)


class Client(models.Model):
    name = models.CharField(max_length=50)


def model_based_upload_to(instance, filename):
    return "opencv/result/{}/{}".format(instance.name, filename)


class ModelOpenCV(models.Model):
    name = models.ForeignKey(Client, on_delete=models.CASCADE)
    confidence = models.CharField(max_length=50, null=True)
    # saved_model = models.FileField(upload_to='opencv/saved_model/', null=True)
    photo = models.ImageField(upload_to=model_based_upload_to)


class DeepFaceUpload(models.Model):
    up_image = models.ImageField(upload_to='deepface/')
