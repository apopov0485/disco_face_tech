from deepface.commons import distance as dst
import os
import numpy as np
import pickle

from deepface import DeepFace
from deepface.basemodels import VGGFace, Facenet

cur = os.getcwd()

DIR = r'./dataset/Faces/val/'
backends = ['opencv', 'ssd', 'dlib', 'mtcnn', 'retinaface']
model = VGGFace.loadModel()


def deepface_verify_distance(path_target_img):
    target_img = DeepFace.detectFace('./media/' + str(path_target_img), enforce_detection=False)
    target_img = target_img.reshape(1, 224, 224, 3)
    target_representation = model.predict(target_img)[0, :]

    # load representations of faces in database
    f = open('representations.pkl', 'rb')
    representations = pickle.load(f)

    distances = []
    for i in range(0, len(representations)):
        source_name = representations[i][0]
        source_representation = representations[i][1]
        distance = dst.findCosineDistance(source_representation, target_representation)
        distances.append(distance)
    # find the minimum distance index
    idx = np.argmin(distances)
    path = representations[idx][0]
    name = path.split('/')[4]
    url = str(path.split('.')[1]) + '.jpg'
    return url.replace(" ", "_"), name


