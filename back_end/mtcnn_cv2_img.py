import cv2
from django.conf import settings
import os
from .models import ImageUpload, ImageResult
from django.core.files.base import ContentFile
from mtcnn_cv2 import MTCNN


def mtcnn_cv2_img(pk, new=False):
    if new:
        detector = MTCNN()

        query = ImageUpload.objects.get(id=pk)
        path = os.path.join(settings.BASE_DIR, 'media/' + str(query.up_image))

        img = cv2.imread(path)
        image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        result = detector.detect_faces(image)
        num_p = []
        if len(result) > 0:
            for id, res in enumerate(result):
                # keypoints = result[0]['keypoints']
                num_p.append(id)
                x, y, w, h = res['box']
                cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), thickness=2)

                cv2.putText(img, f'{int(res["confidence"] * 100)}%',
                            (x, y - 20), cv2.FONT_HERSHEY_PLAIN,
                            2, (255,0,0), 2)
                # cv2.circle(image, (keypoints['left_eye']), 2, (0, 155, 255), 2)
                # cv2.circle(image, (keypoints['right_eye']), 2, (0, 155, 255), 2)
                # cv2.circle(image, (keypoints['nose']), 2, (0, 155, 255), 2)
                # cv2.circle(image, (keypoints['mouth_left']), 2, (0, 155, 255), 2)
                # cv2.circle(image, (keypoints['mouth_right']), 2, (0, 155, 255), 2)
            print(len(num_p))
            ret, buf = cv2.imencode('.jpg', img)
            content = ContentFile(buf.tobytes())

            data = ImageResult()
            data.result = ImageUpload.objects.get(id=pk)
            data.num_people = len(num_p)
            data.mtcnn_cv2 = True
            data.store_image.save(f'mtcnn_cv2_result_{pk}.jpg', content)
