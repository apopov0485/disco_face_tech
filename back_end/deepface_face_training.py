# deepface~=0.0.65

import os
import cv2 as cv
import numpy as np
from django.conf import settings
import pickle
from tqdm import tqdm
from deepface.commons import functions
from deepface import DeepFace
from deepface.basemodels import (VGGFace, OpenFace,
                                 Facenet, FbDeepFace,
                                 Facenet512, DeepID,
                                 ArcFace, DlibWrapper
                                 )

pretrained_models = {}

pretrained_models["VGG-Face"] = VGGFace.loadModel()
print("VGG-Face loaded")
pretrained_models["Facenet"] = Facenet.loadModel()
print("Facenet loaded")
pretrained_models["OpenFace"] = OpenFace.loadModel()
print("OpenFace loaded")
pretrained_models["DeepFace"] = FbDeepFace.loadModel()
print("FbDeepFace loaded")
pretrained_models["Facenet512"] = Facenet512.loadModel()
print("Facenet512 loaded")
pretrained_models["DeepID"] = DeepID.loadModel()
print("DeepID loaded")
pretrained_models["ArcFace"] = ArcFace.loadModel()
print("ArcFace loaded")
pretrained_models["Dlib"] = DlibWrapper.loadModel()
print("Dlib loaded")


models = [ "OpenFace", "ArcFace", "Dlib"]
# models = ["VGG-Face", "Facenet", "Facenet512", "OpenFace", "DeepFace", "DeepID", "ArcFace", "Dlib"] "DeepID",

backends = ['opencv', 'ssd', 'dlib', 'mtcnn', 'retinaface']
people = ['Al Pacino', 'Ben Afflek', 'Elton John', 'Jerry Seinfield', 'Madonna', 'Mindy Kaling']

DIR = r'../dataset/Faces/train/'

# --------------------------
clients = []

for r, d, f in os.walk(DIR):  # r=root, d=directories, f = files
    for file in f:
        if ('.jpg' in file):
            exact_path = r + "/" + file
            clients.append(exact_path)
# --------------------------

for model in tqdm(models):
    representations = []
    for client in clients:
        img = DeepFace.detectFace(client, enforce_detection=False, detector_backend=backends[0])
        # img = img.reshape(1, 224, 224, 3)
        representation = pretrained_models[model].predict(img)
        # representation = pretrained_models[model].predict(img)[0, :]
        instance = []
        instance.append(client)
        instance.append(representation)
        representations.append(instance)
# --------------------------
    f = open(f'../media/deepface/representation/{model}_representations.pkl', "wb")
    pickle.dump(representations, f)
    f.close()



