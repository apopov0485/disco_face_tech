import cv2 as cv
from django.conf import settings
import os
from .models import ImageUpload, ImageResult
from django.core.files.base import ContentFile


def opencv_img(pk, new=False):
    if new:
        face_detection = cv.CascadeClassifier(os.path.join(
            settings.BASE_DIR, 'opencv_haarcascade_data/haarcascade_frontalface_default.xml'))

        query = ImageUpload.objects.get(id=pk)
        path = os.path.join(settings.BASE_DIR, 'media/' + str(query.up_image))

        img = cv.imread(path)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        faces_rect = face_detection.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=8)

        for (x,y,w,h) in faces_rect:
            cv.rectangle(img, (x,y), (x+w,y+h), (0,255,0), thickness=2)

        ret, buf = cv.imencode('.jpg', img)
        content = ContentFile(buf.tobytes())

        data = ImageResult()
        data.result = ImageUpload.objects.get(id=pk)
        data.num_people = len(faces_rect)
        data.opencv = True
        data.store_image.save(f'opencv_result_{pk}.jpg', content)

