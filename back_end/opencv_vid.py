import cv2 as cv
from django.conf import settings
import os
from .models import VideoUpload
from django.core.files.base import ContentFile

face_detection = cv.CascadeClassifier(os.path.join(
    settings.BASE_DIR, 'opencv_haarcascade_data/haarcascade_frontalface_default.xml'))


def fancy_draw(self, img, bbox, l=30, t=5, rt=1):
    x, y, w, h = bbox
    x1, y1 = x + w, y + h

    cv.rectangle(img, bbox, (255, 0, 255), rt)
    # Top Left  x,y
    cv.line(img, (x, y), (x + l, y), (255, 0, 255), t)
    cv.line(img, (x, y), (x, y + l), (255, 0, 255), t)
    # Top Right  x1,y
    cv.line(img, (x1, y), (x1 - l, y), (255, 0, 255), t)
    cv.line(img, (x1, y), (x1, y + l), (255, 0, 255), t)
    # Bottom Left  x,y1
    cv.line(img, (x, y1), (x + l, y1), (255, 0, 255), t)
    cv.line(img, (x, y1), (x, y1 - l), (255, 0, 255), t)
    # Bottom Right  x1,y1
    cv.line(img, (x1, y1), (x1 - l, y1), (255, 0, 255), t)
    cv.line(img, (x1, y1), (x1, y1 - l), (255, 0, 255), t)
    return img


def opencv_vid(pk):
    query = VideoUpload.objects.get(id=pk)
    path = os.path.join(settings.BASE_DIR, 'media/' + str(query.video_file))

    video = cv.VideoCapture(path)
    while True:

        success, img = video.read()
        if not success:
            print("Can't receive frame")
            break
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        faces_rect = face_detection.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=3)

        for (x,y,w,h) in faces_rect:
            cv.rectangle(img, (x,y), (x+w,y+h), (0,255,0), thickness=2)
        cv.imshow('frame', gray)
        if cv.waitKey(1) == ord('q'):
            break
        # return img
    video.release()
    cv.destroyAllWindows()









# def get_frame(self):
#     success, image = self.video.read()
#     gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
#     faces_detected = face_detection_videocam.detectMultiScale3(gray, scaleFactor=1.3, minNeighbors=3,
#                                                                outputRejectLevels=True)
#
#     for (x, y, w, h) in faces_detected[0]:
#         bbox = x, y, w, h
#         self.fancy_draw(image, bbox)
#     frame_flip = cv.flip(image, 1)
#     ret, jpeg = cv.imencode('.jpg', frame_flip)
#     return jpeg.tobytes()