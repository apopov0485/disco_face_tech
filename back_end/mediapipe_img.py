import cv2 as cv
from django.conf import settings
import os
from .models import ImageUpload, ImageResult
from django.core.files.base import ContentFile

import cv2
import mediapipe as mp


def mediapipe_img(pk, new=False):
    if new:
        query = ImageUpload.objects.get(id=pk)
        path = os.path.join(settings.BASE_DIR, 'media/' + str(query.up_image))

        img = cv.imread(path)
        mp_face_detection = mp.solutions.face_detection
        mp_draw = mp.solutions.drawing_utils
        face_detection = mp_face_detection.FaceDetection(0.5, model_selection=1)

        img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        results = face_detection.process(img_rgb)

        num_p =[]
        if results.detections:
            for id, detection in enumerate(results.detections):
                # mpDraw.draw_detection(img, detection)
                num_p.append(id)
                bboxC = detection.location_data.relative_bounding_box
                ih, iw, ic = img.shape
                bbox = int(bboxC.xmin * iw), int(bboxC.ymin * ih), int(bboxC.width * iw), int(bboxC.height * ih)
                cv2.rectangle(img, bbox, (255, 0, 255), 2)
                cv2.putText(img, f'{int(detection.score[0] * 100)}%',
                            (bbox[0], bbox[1] - 20), cv2.FONT_HERSHEY_PLAIN,
                            2, (255, 0, 255), 2)
        ret, buf = cv.imencode('.jpg', img)
        content = ContentFile(buf.tobytes())
        data = ImageResult()
        data.result = ImageUpload.objects.get(id=pk)
        data.num_people = len(num_p)
        data.mediapipe = True
        data.store_image.save(f'mediapipe_result_{pk}.jpg', content)
