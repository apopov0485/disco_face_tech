import pandas as pd
import numpy as np
from sklearn import metrics
from sklearn.metrics import confusion_matrix, accuracy_score, roc_curve, auc, classification_report, ConfusionMatrixDisplay
import matplotlib.pyplot as plt
from tqdm import tqdm
import pickle
from deepface.commons import distance as dst
import os
import seaborn as sns
tqdm.pandas()

from deepface import DeepFace
from deepface.basemodels import (VGGFace, OpenFace,
                                 Facenet, FbDeepFace,
                                 Facenet512, DeepID,
                                 ArcFace, DlibWrapper
                                 )

pretrained_models = {}

pretrained_models["VGG-Face"] = VGGFace.loadModel()
print("VGG-Face loaded")
pretrained_models["Facenet"] = Facenet.loadModel()
print("Facenet loaded")
pretrained_models["Facenet512"] = Facenet512.loadModel()
print("Facenet512 loaded")
pretrained_models["DeepFace"] = FbDeepFace.loadModel()
print("FbDeepFace loaded")
# pretrained_models["OpenFace"] = OpenFace.loadModel()
# print("OpenFace loaded")
# pretrained_models["DeepID"] = DeepID.loadModel()
# print("DeepID loaded")
# pretrained_models["ArcFace"] = ArcFace.loadModel()
# print("ArcFace loaded")
# pretrained_models["Dlib"] = DlibWrapper.loadModel()
# print("Dlib loaded")



models = ["VGG-Face", "Facenet", "Facenet512", "DeepFace"]
# models = ["VGG-Face", "Facenet", "Facenet512", "OpenFace", "DeepFace", "DeepID", "ArcFace", "Dlib"]  , 'mediapipe'
people = ['al_pacino', 'alexandre_popov', 'ben_afflek', 'elton_john', 'jerry_seinfield', 'madonna', 'mindy_kaling']

DIR = r'../../dataset/Faces/val/'
backends = ['opencv', 'ssd', 'dlib', 'mtcnn', 'retinaface']

clients = []

for r, d, f in os.walk(DIR):  # r=root, d=directories, f = files
    for file in f:
        if ('.jpg' in file):
            exact_path = r + "/" + file
            clients.append(exact_path)

df = []
for model in tqdm(models, desc='models', colour='green'):
    for backend in backends:
        for client in clients:
            target_img = DeepFace.detectFace(client, enforce_detection=False, detector_backend=backend)
            target_img = target_img.reshape(1, 224, 224, 3)
            target_representation = pretrained_models[model].predict(target_img)[0, :]
            # load representations of faces in database
            f = open(f'../../media/deepface/representation/{model}_representations.pkl', 'rb')
            representations = pickle.load(f)
            distances = []
            for i in range(0, len(representations)):
                source_name = representations[i][0]
                source_representation = representations[i][1]
                distance = dst.findCosineDistance(source_representation, target_representation)
                distances.append(distance)
            # find the minimum distance index
            idx = np.argmin(distances)
            distance = np.amin(distances)
            path = representations[idx][0]
            label = str(path.split('.')[2])
            label = label.split('/')[4].replace(' ', '_').lower()
            name = client.split('/')[5]
            if name == label:
                df.append([name, client, distance, label, 1, model, backend])
            else:
                df.append([name, client, distance, label, 0, model, backend])

df = pd.DataFrame(df, columns=["person", "path", "distance", "predict", 'result', 'model', 'backend_detector'])


def classification_report_csv(report, model, backend):
    idx = df.index[(df['model'] == model) & (df['backend_detector'] == backend)].tolist()
    df.loc[df.index[idx], 'accuracy'] = report['accuracy']

def cm():
    plt.figure(figsize=(20, 20))
    for model in tqdm(models):
        df_model = df.loc[df['model'] == model]
        for backend in backends:
            df_backend = df_model.loc[df['backend_detector'] == backend]
            cm_plot = confusion_matrix(df_backend['person'], df_backend['predict'], labels=people)
            disp = ConfusionMatrixDisplay(confusion_matrix=cm_plot,
                                          display_labels=people)
            disp.plot()
            plt.xlabel(people)
            # # plt.xlabel("Predicted labels")
            plt.ylabel(people)
            # # plt.ylabel("True labels")
            plt.xticks(rotation=20)
            # plt.yticks([], [])
            plt.title(f'Confusion matrix model : {model}, detector : {backend}')
            plt.savefig(f'./metric_img/{model}_cm_deepface.jpg')
            report = classification_report(df_backend['person'], df_backend['predict'], output_dict=True)
            classification_report_csv(report, model, backend)


def df_plt():
    plt.figure(figsize=(10, 10))
    for model in tqdm(models):
        df_model = df.loc[df['model'] == model]
        for backend in backends:
            df_backend = df_model.loc[df['backend_detector'] == backend]
            sns.set_theme(style="darkgrid")
            sns.countplot(x="result", data=df_backend)
            plt.title(f'Negative vs positive model : {model}, detector : {backend}')
            # df["result"].value_counts().plot.bar(x='negative', y="positive", rot=0, color='red')
            plt.savefig(f'./metric_img/{model}_{backend}_bar_deepface.jpg')


def plot_best_model():
    df['accuracy'].plot()
    plt.savefig('deepface.jpg')


if __name__ == '__main__':
    df_plt()
    cm()
    plot_best_model()
    df.to_csv("all_model_deepface.csv", index=False)
# y_pred_proba = df['predict'].to_list()
# y_test = df['person'].to_list()
# # df['person'], df['predict']
# fpr, tpr, _ = metrics.roc_curve(y_test, y_pred_proba, pos_label=people)
# auc = metrics.roc_auc_score(y_test, y_pred_proba)
#
# plt.figure(figsize=(7, 3))
# plt.plot(fpr, tpr, label="data 1, auc=" + str(auc))
# plt.savefig('roc_curve_cv.jpg')

# import matplotlib.image as mpimg
# plt.figure(figsize=(15, 15))
# for i, client in enumerate(clients):
#     images = mpimg.imread(client)
#     if (len(clients) / 2) <= i:
#         ax = plt.subplot(int((len(clients) / 2)), 5, i + 1)
#         plt.imshow(images)
#         plt.title(client.split('/')[5])
#         plt.axis("off")
#         plt.savefig('t.jpeg')
#     plt.imshow(images)
#     plt.title(client.split('/')[5])
#     plt.axis("off")
#     plt.savefig('1.jpeg')