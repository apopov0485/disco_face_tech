import pandas as pd
import numpy as np
from sklearn import metrics
from sklearn.metrics import confusion_matrix, accuracy_score, roc_curve, auc, classification_report, ConfusionMatrixDisplay
import matplotlib.pyplot as plt
from tqdm import tqdm
import cv2 as cv
import os

tqdm.pandas()

haar_cascade = cv.CascadeClassifier('../../opencv_haarcascade_data/haarcascade_frontalface_default.xml')

people = ['al_pacino', 'ben_afflek', 'elton_john', 'jerry_seinfeld', 'madonna', 'mindy_kaling']

face_recognizer = cv.face.LBPHFaceRecognizer_create()
face_recognizer.read('../../media/face_trained.yml')
DIR = '../../dataset/Faces/val/'

clients = []

for r, d, f in os.walk(DIR):  # r=root, d=directories, f = files
    for file in f:
        if ('.jpg' in file):
            exact_path = r + "/" + file
            clients.append(exact_path)

df = []
for client in clients:
    img = cv.imread(client)

    if img is not None:

        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        # Detect the face in the image
        faces_rect = haar_cascade.detectMultiScale(gray, 1.1, 4)
        conf = ''
        lab = ''
        for (x, y, w, h) in faces_rect:
            faces_roi = gray[y:y + h, x:x + w]

            label, confidence = face_recognizer.predict(faces_roi)
            conf = confidence
            lab = str(people[label])
        name = client.split('/')[5]
        if name == lab:
            df.append([name, client, conf, lab, 1])
        else:
            df.append([name, client, conf, lab, 0])

df = pd.DataFrame(df, columns=["person", "path", "confidence", "predict", 'result'])
# df.to_csv("face-recognition.csv", index=False)

import matplotlib.image as mpimg
# plt.figure(figsize=(15, 15))
# for i, client in enumerate(clients):
#     images = mpimg.imread(client)
#     if (len(clients) / 2) <= i:
#         ax = plt.subplot(int((len(clients) / 2)), 5, i + 1)
#         plt.imshow(images)
#         plt.title(client.split('/')[5])
#         plt.axis("off")
#         plt.savefig('t.jpeg')
#     plt.imshow(images)
#     plt.title(client.split('/')[5])
#     plt.axis("off")
#     plt.savefig('1.jpeg')


plt.figure(figsize=(10, 10))
cm_plot = confusion_matrix(df['person'], df['predict'], labels=people)
disp = ConfusionMatrixDisplay(confusion_matrix=cm_plot,
                              display_labels=people)
disp.plot()

plt.xlabel(people)
# # plt.xlabel("Predicted labels")
plt.ylabel(people)
# # plt.ylabel("True labels")
plt.xticks(rotation=20)
# plt.yticks([], [], rotation=20)
plt.title('Confusion matrix')
plt.savefig('cm_cv.jpg')

print(classification_report(df['person'], df['predict']))


def df_plt():
    import seaborn as sns
    plt.figure(figsize=(10, 10))
    print(df["result"].value_counts())
    sns.set_theme(style="darkgrid")
    sns.countplot(x="result", data=df
                 )
    plt.title('Negative vs positive')
    # df["result"].value_counts().plot.bar(x='negative', y="positive", rot=0, color='red')
    plt.savefig('bar_cv.jpg')


df_plt()


# y_pred_proba = df['predict'].to_list()
# y_test = df['person'].to_list()
# # df['person'], df['predict']
# fpr, tpr, _ = metrics.roc_curve(y_test, y_pred_proba, pos_label=people)
# auc = metrics.roc_auc_score(y_test, y_pred_proba)
#
# plt.figure(figsize=(7, 3))
# plt.plot(fpr, tpr, label="data 1, auc=" + str(auc))
# plt.savefig('roc_curve_cv.jpg')