# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from front_end import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    # The home page
    path('', views.index, name='home'),

    # Webcam feed and take screenshot
    path('opencv_cam', views.opencv_cam, name='opencv_cam'),
    path('mediapipe_cam', views.mediapipe_cam, name='mediapipe_cam'),
    path('mtcnn_cam', views.mtcnn_cam, name='mtcnn_cam'),
    path('video_feed_opencv', views.webcam_feed_opencv, name='video_feed_opencv'),
    path('video_feed_mediapipe', views.webcam_feed_mediapipe, name='video_feed_mediapipe'),
    path('video_feed_mtcnn', views.webcam_feed_mtcnn, name='video_feed_mtcnn'),
    path('take_screenshot', views.take_screenshot, name='take_screenshot'),

    # Matches any html file
    # re_path(r'^.*\.*', views.pages, name='pages'),
    path('upload_image', views.UploadImage.as_view(), name='upload_image'),
    path('upload_video', views.upload_video, name='upload_video'),
    path('image', views.image, name='image'),
    path('LBPHFaceRecognizer', views.opencv_facerecognizer, name='LBPHFaceRecognizer'),
    path('deepface', views.deepface, name='deepface'),

    path('image/<int:pk>/', views.ImageDisplay.as_view(), name='image_display'),
    path('video/<int:pk>/', views.video_display, name='video_display'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.DATASET_URL, document_root=settings.DATASET)
