# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

import os

from django.shortcuts import render, redirect
from django.template import loader
from django.http import HttpResponse
from django import template
from django.http.response import StreamingHttpResponse
from django.conf import settings
from back_end.camera import VideoCamera, FaceDetector, FaceDetectorMTCNN
from django.views.generic import DetailView
from back_end.models import ImageUpload, ImageResult, VideoUpload
from back_end.opencv_img import opencv_img
from back_end.mediapipe_img import mediapipe_img
from back_end.mtcnn_cv2_img import mtcnn_cv2_img
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from back_end.forms import ImageUploadForm, VideoForm, DeepFaceUploadForm
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
from back_end.deepface_rep import deepface_verify_distance
from back_end.opencv_vid import opencv_vid


# @login_required(login_url="/login/")
def index(request):
    
    context = {}
    context['segment'] = 'index'

    html_template = loader.get_template( 'index.html' )
    return HttpResponse(html_template.render(context, request))


# @login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:
        
        load_template      = request.path.split('/')[-1]
        context['segment'] = load_template
        
        html_template = loader.get_template( load_template )
        return HttpResponse(html_template.render(context, request))
        
    except template.TemplateDoesNotExist:

        html_template = loader.get_template( 'page-404.html' )
        return HttpResponse(html_template.render(context, request))

    except:
    
        html_template = loader.get_template( 'page-500.html' )
        return HttpResponse(html_template.render(context, request))


def opencv_cam(request):
    return render(request, 'opencv_cam.html')


@csrf_exempt
def take_screenshot(request):
    camera = VideoCamera()
    screenshot = request.POST.get('result')
    if screenshot == 'screenshot':
        # data = {'screenshot': True}
        screenshot = True
        camera.take_screenshot(screenshot=screenshot)
        return JsonResponse(screenshot, safe=False)


def mediapipe_cam(request):
    return render(request, 'mediapipe_cam.html')


def mtcnn_cam(request):
    return render(request, 'mtcnn_cam.html')


def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


def gen_mp(camera):
    while True:
        frame = camera.get_frame_mp()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


def gen_mtcnn(camera):
    while True:
        frame = camera.get_frame_mtcnn()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


def webcam_feed_opencv(request):
    return StreamingHttpResponse(gen(VideoCamera()),
                                 content_type='multipart/x-mixed-replace; boundary=frame')


def webcam_feed_mediapipe(request):
    return StreamingHttpResponse(gen_mp(FaceDetector()),
                                 content_type='multipart/x-mixed-replace; boundary=frame')


def webcam_feed_mtcnn(request):
    return StreamingHttpResponse(gen_mtcnn(FaceDetectorMTCNN()),
                                 content_type='multipart/x-mixed-replace; boundary=frame')


class UploadImage(TemplateView):
    form = ImageUploadForm
    template_name = 'image_upload.html'

    def post(self, request, *args, **kwargs):
        form = ImageUploadForm(request.POST, request.FILES)

        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect(reverse_lazy('image_display', kwargs={'pk': obj.id}))

        context = self.get_context_data(form=form)
        return self.render_to_response(context)

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


def upload_video(request):
    form = VideoForm
    if request.method == 'POST':
        form = VideoForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            obj = form.save()
            return HttpResponseRedirect(reverse_lazy('video_display', kwargs={'pk': obj.id}))
    context = {'form': form}
    return render(request, 'video_upload.html', context)


def video_display(request, pk):
    query = VideoUpload.objects.get(id=pk)
    context = {'vid': query, 'opencv': opencv_vid(pk)}
    return render(request, 'video_display.html', context)


class ImageDisplay(DetailView):
    model = ImageUpload
    template_name = 'image_display.html'
    context_object_name = 'img'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['img']
        query = ImageResult.objects.filter(result=obj.id)
        context['results'] = query
        if not query.exists():
            opencv_img(obj.id, True)
            mediapipe_img(obj.id, True)
            mtcnn_cv2_img(obj.id, True)
        # elif query.exists():

        return context


def image(request):
    query = ImageUpload.objects.all()
    context = {'imgs': query}
    return render(request, 'image.html', context)

# Face recognition


def deepface(request):
    """Process images uploaded by users"""

    dataset_url = settings.DATASET_URL
    models = ["VGG-Face", "Facenet", "Facenet512", "DeepFace"]
    backends = ['opencv', 'ssd', 'dlib', 'mtcnn', 'retinaface']
    if request.method == 'POST':
        form = DeepFaceUploadForm(request.POST or None, request.FILES or None)

        print(form.data)
        if form.is_valid():
            print(form.cleaned_data)
            form.save()
            # Get the current instance object to display in the template
            img_obj = form.instance
            img_url, name = deepface_verify_distance(img_obj.up_image)
            return render(request, 'deepface.html', {'form': form, 'img_obj': img_obj, 'deepface_closest_img': img_url,
                                                     'recognised_name': name, 'models': models, 'backends': backends
                                                     })
    else:
        form = DeepFaceUploadForm
    context = {'form': form, 'models': models, 'backends': backends}
    return render(request, 'deepface.html', context)


def opencv_facerecognizer(request):
    data = []
    DIR = os.path.join(settings.MEDIA_ROOT, 'opencv/result/')
    for r, d, f in os.walk(DIR):
        for file in f:
            url = str(settings.MEDIA_URL) + 'opencv/result/' + str(file)
            name, confidence = file.split('-')
            data.append({'url': url, 'name': str(name).replace('_', ' '),
                         'confidence': '.'.join(str(confidence).split(".", 2)[:2])
                         })
    context = {'data': data}
    return render(request, 'LBPHFaceRecognizer.html', context)

